﻿using System;
using System.IO;
using System.Linq;
using PomoNote.Abstract;
using PomoNote.Data;

namespace PomoNote.Persistence
{
    public class FileBasedNotePersistor : INotePersistor
    {
        private readonly PersistorConfig _config;

        public FileBasedNotePersistor(Action<PersistorConfig> config)
        {
            _config = new PersistorConfig();
            config?.Invoke(_config);

            if (!Directory.Exists(_config.BaseDirectory))
                Directory.CreateDirectory(_config.BaseDirectory);
        }


        public string PersistNote(string name, string content)
        {
            string file;
            if(name.Any(c=> Path.GetInvalidFileNameChars().Any(i=> i==c)))
            {

                file = Path.GetRandomFileName();
                file = new string(name.Take(8).ToArray());
            }
            else
            {
                file = name;
            }
            var date = DateTime.Now.ToString("ddd dd-MM-yyyy hh-mm-ss");

            
            var filename = _config.TimeStampNames ? $"{file} {date}.txt" : $"{file}.txt";

            var fullPath = Path.Combine(_config.BaseDirectory, filename);
            var increment = 0;
            while(true)
            {

                if (File.Exists(fullPath))
                {
                    increment++;
                    filename = $"{file} {date}({increment}).txt";
                    fullPath = Path.Combine(_config.BaseDirectory, filename);
                }
                else
                {
                    break;
                }
            }

            var template =
                "Note Title: <name>\r\n" +
                "Date: <date>\r\n" +
                "---------------------------------------------------------------------------------------------------------------------------------------------------\r\n" +
                "<content>";


            if (template != null)
            {
                if (template.Contains("<name") && template.Contains("<date>") && template.Contains("<content>"))
                {
                    template = template.Replace("<name>", name);
                    template = template.Replace("<date>", date);
                    template = template.Replace("<content>", content);
                    content = template;
                }
            }
            using (var sw = new StreamWriter(fullPath))
            {
                sw.Write(content);
            }
            return filename;
        }
    }


}