﻿using System;
using PomoNote.Commands;
using PomoNote.Manager;
using PomoNote.Persistence;

namespace PomoNote
{
    class Program
    {
        static void Main()
        {
            Console.BackgroundColor = ConsoleColor.Blue;
            Console.Clear();
            Console.Title = "PomoNote";

            var cm = new CommandManager(c=>
            {
                c.Prompt = "=>";
            });

            cm.Add(c => c.Add(new NoteCommand("new", new FileBasedNotePersistor(pc =>
            {
                pc.BaseDirectory = "Notes";
                pc.TimeStampNames = false;
            }))));


            while (true)
            {
                cm.GetAndProcessCommand();
            }
        }
    }
}
