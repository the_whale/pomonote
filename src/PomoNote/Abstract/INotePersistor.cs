﻿using PomoNote.Abstract;

namespace PomoNote.Abstract
{
    public interface INotePersistor 
    {
        string PersistNote(string name,string content);
    }
}