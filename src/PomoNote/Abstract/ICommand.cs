﻿namespace PomoNote.Abstract
{
    public interface ICommand
    {
        string Name { get; }
        int MinArgCount { get; }
        int MaxArgCount { get; }
        void Execute(string[] args);
    }
}