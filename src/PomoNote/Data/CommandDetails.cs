﻿namespace PomoNote.Data
{
    public class CommandDetails
    {
        public string Command { get; private set; } 
        public string[] Args { get; private set; }

        public CommandDetails(string command, string[] args)
        {
            Command = command;
            Args = args;
        }
    }
}