﻿using System.IO;

namespace PomoNote.Data
{
    public class PersistorConfig
    {
        public string BaseDirectory { get; set; }
        public bool TimeStampNames { get; set; }

        public PersistorConfig()
        {
            BaseDirectory = "Notes";
            TimeStampNames = true;
        }
    }
}