﻿using System;
using System.Linq;
using PomoNote.Data;

namespace PomoNote.Manager
{
    public class CommandParser
    {
        public CommandDetails LastValidCommand { get; private set; }
        public bool HasValidCommand => LastValidCommand == null;

        public bool RequestCommand()
        {
            var command = Console.ReadLine();


            if (!ValidateCommand(command))
                return false;

            var splitCommand = command.Split();
            LastValidCommand = new CommandDetails(splitCommand.First().ToLowerInvariant(),splitCommand.Skip(1).Select(s => s.ToLowerInvariant()).ToArray());
            return true;
        }

        private static bool ValidateCommand(string command)
        {
            return !string.IsNullOrWhiteSpace(command) && !command.Any(char.IsSymbol);
        }
    }
}