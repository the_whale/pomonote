﻿using System;
using System.Collections.Generic;
using System.Linq;
using PomoNote.Abstract;

namespace PomoNote.Manager
{
    public class CommandManager
    {
        private readonly CommandParser _parser;
        private readonly List<ICommand> _commands;
        private readonly CommandManagerConfigurator _config;

        public CommandManager(Action<CommandManagerConfigurator> configProvider = null )
        {

            _config = new CommandManagerConfigurator();
            configProvider?.Invoke(_config);
            _parser=new CommandParser();
            _commands = new List<ICommand>();
   
        }

        public CommandManager Add(Action<CommandAdder> commandAdder)
        {
            var adder = new CommandAdder();
            commandAdder?.Invoke(adder);
            _commands.AddRange(adder.Commands);
            return this;
        }

      
        public void GetAndProcessCommand()
        {
            Console.WriteLine("Welcome to PomoNote! Type new or new <name> to start a new note.");
            Console.Write(_config.Prompt);
            var validCommand = _parser.RequestCommand();
            if (!validCommand)
            {
                Console.WriteLine(_config.InvalidCommandError);
                return;
            }


            var command = _parser.LastValidCommand;

            var iCommand = _commands.FirstOrDefault(c => c.Name == command.Command && c.MinArgCount <= command.Args.Length && c.MaxArgCount >= command.Args.Length);
            if (iCommand!=null)
            {
                iCommand.Execute(command.Args);
            }
            else
            {
                Console.WriteLine(_config.CommandNotFoundError);
            }


        }


        
    }

    public class CommandAdder
    {
        public List<ICommand> Commands { get; private set; }
        

        public CommandAdder()
        {
            Commands = new List<ICommand>();
        
        }

        public CommandAdder Add(ICommand command)
        {
            Commands.Add(command);
            return this;
        }
    }
    public class CommandManagerConfigurator
    {
        public string Prompt { get; set; }
        public string CommandNotFoundError { get; set; }
        public string InvalidCommandError { get; set; }
        public string InvalidArgumentError { get; set; }
        public ConsoleColor ForeGroundColor { get; set; }
        public ConsoleColor BackgroundColor { get; set; }

        public CommandManagerConfigurator()
        {
            Prompt = "->";
            CommandNotFoundError = "Command Not Found";
            InvalidCommandError = "Invalid Command";
            InvalidArgumentError = "Invalid Arguments";
            ForeGroundColor = ConsoleColor.White;
            BackgroundColor = ConsoleColor.Black;
        }
    }
}