﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using PomoNote.Abstract;
using PomoNote.Manager;

namespace PomoNote.Commands
{
    public class NoteCommand : CommandManager, ICommand
    {
        private readonly INotePersistor _persistor;
        public string Name { get; }
        public int MinArgCount { get; }
        public int MaxArgCount { get; }


        public NoteCommand(string name,INotePersistor persistor, Action<CommandManagerConfigurator> configProvider = null) : base(configProvider)
        {
            _persistor = persistor;
            Name = name;
            MinArgCount = 0;
            MaxArgCount = 1;
        }

        public void Execute(string[] args)
        {
            string name;
            if (args.Length == 0)
            {
                name = Path.GetRandomFileName();
                name = new string(name.Take(8).ToArray());
            }
            else
            {
                name = args[0];
            }
            Console.Clear();
            Console.WriteLine($"Writing note '{name}'. Type your note. When done, type \"done\" on a new line     without quotes to finish your note.");
            Console.Write("================================================================================");

            var sb = new StringBuilder();
            while (true)
            {
                var text = Console.ReadLine();
                if (text == "done")
                    break;
                sb.AppendLine(text);
                
            }
            var savedAs =_persistor.PersistNote(name,sb.ToString());
            Console.WriteLine($"File saved as {savedAs}. Hit any key to continue.");
            Console.ReadKey(true);
            Console.Clear();
        }
    }
}