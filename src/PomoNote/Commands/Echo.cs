﻿using System;
using System.Linq;
using PomoNote.Abstract;

namespace PomoNote.Commands
{
    public class Echo : ICommand
    {
        public Echo()
        {
            Name = "echo";
            MinArgCount = 1;
            MaxArgCount = -1;
        }

        public string Name { get; }
        public int MinArgCount { get; }
        public int MaxArgCount { get; }
        

        public void Execute(string[] args)
        {
            if (args.Length == 0)
                return;
            Console.WriteLine(args.Aggregate((current,next)=> $"{current} {next}"));
        }
    }
}